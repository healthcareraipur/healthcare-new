package com.aayushi.hch.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.aayushi.hch.entity.Prescription;
import com.aayushi.hch.utility.PDFUtility;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
@Controller
public class MedicineController {

	@Autowired
	PDFUtility pdfUtility;
	@RequestMapping(method = RequestMethod.GET, value = "/medicine")
	public ModelAndView getRegister() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("DynamicTable");
		return modelAndView;

	}
	@RequestMapping(method = RequestMethod.POST, value = "/saveMedicine")
	public ModelAndView addUser(@RequestBody String prescription) throws Exception{
		//Employee employeeFromJson = objectMapper.readValue(movie, Employee.class);
	
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
		System.out.println(prescription);
		System.out.println("Prescription#################################\n\n");
		Prescription pres =objectMapper.readValue(prescription, Prescription.class);
		System.out.println(pres);
		String pdfPath = "e:/" + pres.getPatientName() + ".pdf";
		pdfUtility.generatePdf(pres, pdfPath);
		//emailUtility.sendPdf(report.getUser().getEmail(), pdfPath);
		return new ModelAndView("redirect:" + "/register");
	}
}
