<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/Doctor.css"/>
</head>
<body>
<nav class="navbar navbar-inverse" id="navbar">
	<div class="container-fluid">
		<div class="navbar-header">
			
		</div>
		<h3 style="text-align: center;"> <font color="Black" size="5px" weight="bold">	<em>Upcoming date  </em></font></h3>
	</div>
	</nav>
	<div class="container" style="width: 500px;">

		<form action="upcomingAppointment" method="post" modelAttribute="userId"   modelAttribute="dateDto" >
		<div class="form-group">
		<input type="hidden"
					class="form-control" id="userId" value="${userId}" name="userId">

			<div class="form-group">
				<label for="appointment_date">Appointment Date:</label> <input type="date"
					class="form-control" id="appointment_date"
					name="appointment_date"  >
			</div>
			

			

			<button type="submit"  value="upcomingAppointment">Submit</button>
		</form>
	</div>



</body>

</html>