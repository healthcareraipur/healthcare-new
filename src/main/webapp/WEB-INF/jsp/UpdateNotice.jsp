<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/Doctor.css"/>
</head>
<body>
<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			
		</div>
		<h3 style="text-align: center"><font color="black">	Edit Notice</font></h3>
	</div>
	</nav>
	<div class="container" style="top:60%;">

		<form action="/update" method="post" modelAttribute="noticeDto">
			<input type="hidden"
					class="form-control" id="id" name="id" value="${notice.id}"required>
			
			<%-- <input type="hidden"
					class="form-control" id="userId" value="${userId}" name="user.id">
 --%>
			<div class="form-group">
				<font color="red">*</font><label for="Subject">Subject:</label> <input type="text"
					class="form-control" id="Subject" placeholder="Enter Subject"
					name="subject" value="${notice.subject}"required>
			</div>
			
			<div class="form-group">
				<font color="red">*</font><label for="startDate">Start Date:</label> <input type="date"
					class="form-control" id="startDate"
					name="startDate" value="${notice.startDate}"required>
			</div>
<div class="form-group">
				<font color="red">*</font><label for="endDate">End Date:</label> <input type="date"
					class="form-control" id="endDate"
					name="endDate" value="${notice.endDate}"required>
			</div>
			<div class="form-group">
				<font color="red">*</font><label for="venue">Venue:</label> <input type="text"
					class="form-control" id="venue"
					placeholder="Enter venue" name="venue" value="${notice.venue}"required>
			</div>
			
			<div class="form-group">
				<font color="red">*</font><label for="documents">Documents:</label> <input type="text"
					class="form-control" id="documents"
					placeholder="Enter documents" name="documents" value="${notice.documents}"required>
			</div>
			
			<div class="form-group">
				<font color="red">*</font><label for="description">Description:</label> <input type="text"
					class="form-control" id="description"
					placeholder="Enter description" name="description" value="${notice.description}" required>
			</div>
			
			<button type="submit"  value="update">Submit</button>
		</form>
	</div>
</body>
</html>