<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>


<script type="text/javascript">
function logout() {
var answer = confirm ("you have successfully logged off , click on OK to continue.")
if (answer)
window.location="http://localhost:8080/loginHere";
}
</script>
<!-- javascript:AlertIt(); -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/Comment.css"/>
<title>Insert title here</title>
</head>
<body>
	<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			
		</div>
		<ul class="nav navbar-nav">
			<li class="active"><a href="#">Home</a></li>
			<li><a href="appointment?userId=${userId}">New Appointment</a></li>
			<li><a href="UserNotice">Notices</a></li>
			<li><a href="profileEdit?userId=${userId}">Edit Profile</a></li>
		
		</ul >
		
		<h4 style="text-align: right"><a href="javascript:logout();" style ="color : black">Logout</a></h4>
		
	</div>
	</nav>
	
           <h3 ><center><font color="lightblue"> Have Any Query?</font></center></h3>
           <h5 ><center><font color="black">(drop your question in this box right below)</font></center></h5>
           <hr>
           <div class="container1" >
 

		<form action="saveComment" method="post" modelAttribute="commentDto">

			<input type="hidden"
					class="form-control" id="createdBy" value="${userId}" name="createdBy">
		<div class="container1">
			<div class="form-group">
	
		<label for="topic">Topic:</label> <input type="text"
					class="form-control" id="topic"
					placeholder="Enter topic" name="topic"  required>
			</div>
			
			<div class="form-group">
			<label for="description">Description:</label> <input type="text"
					class="form-control" id="description"
					placeholder="Enter description" name="description"required>
			</div>

		<center>	<button type="submit" value="saveComment"><b>Submit</b></button></center>
			</div>
		</form>
	</div>

           
	<div class="container" >

		
				<c:forEach var="comment" items="${comments}">
					
					<b><font color="blue">	${comment.createdBy.fullName}</font></b> 	
					<font color="grey"><b>Topic</b> :${comment.topic}<br></font>
					<b>Description</b>: ${comment.description}<br>
				
					<font color="grey"><h6><b>
					<a href="deleteAppointment?id=${appointment.appointmentId}"  onclick="return confirm('Are you sure you want to delete this item?');"><div class="glyphicon glyphicon-trash"></div></a>
					  <a href="deleteAppointment?id=${appointment.appointmentId}"  onclick="return confirm('Are you sure you want to delete this item?');"><div class="glyphicon glyphicon-pencil"></div></a>
					 <a href="replies?commentId=${comment.commentId}&userId=${userId}"><div class="glyphicon glyphicon-eye-open"></div></a>
					Created On</b>:	${comment.createdOn}
					<b>Modified On</b>:	${comment.modifiedOn}
					    <br>
					     
					<br><br></h6></font>

				</c:forEach>

	</div>
</body>
</html>