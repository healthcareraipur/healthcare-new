package com.aayushi.hch.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.aayushi.hch.entity.User;
import com.aayushi.hch.entity.UserType;

public interface UserRepository extends JpaRepository<User, Long>{

	public User findByEmailAndPassword(String email, String password);
	
	public List<User> findByUsertype(UserType usertype);
	
}
