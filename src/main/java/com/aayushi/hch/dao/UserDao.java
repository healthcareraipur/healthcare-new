package com.aayushi.hch.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.aayushi.hch.entity.User;
import com.aayushi.hch.entity.UserType;
import com.aayushi.hch.repository.UserRepository;

@Repository
public class UserDao {
	@Autowired
	private UserRepository userRepository;

	public void saveUser(User user) {
		userRepository.save(user);
	}

	public User getById(Long id) {
		return userRepository.findOne(id);
	}
	
	public User findByEmailAndPassword(String email , String password) {
		return userRepository.findByEmailAndPassword(email, password);
	}

	public void updateUser(User user, Long id) {
		User findOne = userRepository.findOne(id);
		if (findOne == null) {
			throw new IllegalArgumentException("User doesn't exist with id=" + id);
		}
		user.setId(id);
        userRepository.save(user);
		
	}

	public void deleteUser(Long userId) {
		userRepository.delete(userId);
		
	}
	public List<User> getAllUser() {
		return userRepository.findByUsertype(UserType.PATIENT);
	}
}
