package com.aayushi.hch.request;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class CommentDto {

	private Long commentId;
	
	private String topic;
	private String description;
	private Long createdBy;
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date createdOn;
	
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date modifiedOn;
	
	
	@Override
	public String toString() {
		return "CommentDto [commentId=" + commentId + ", topic=" + topic + ", description=" + description
				+ ", createdBy=" + createdBy + ", createdOn=" + createdOn + ", modifiedOn=" + modifiedOn + "]";
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public Long getCommentId() {
		return commentId;
	}
	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	
	
}
