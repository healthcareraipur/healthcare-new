package com.aayushi.hch.request;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.aayushi.hch.entity.DoctorSpeciality;
import com.aayushi.hch.entity.Gender;
import com.aayushi.hch.entity.UserType;



public class UserRequestDto {
	private Long id;
	private String fullName;

	private String email;

	private String password;

	private String usertype;

	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date dob;

	private String mobile;

	private String gender;

	private Long doctorSpeciality;

	@Override
	public String toString() {
		return "UserRequestDto [id=" + id + ", fullName=" + fullName + ", email=" + email + ", password=" + password
				+ ", usertype=" + usertype + ", dob=" + dob + ", mobile=" + mobile + ", gender=" + gender
				+ ", doctorSpeciality=" + doctorSpeciality + ", weight=" + weight + ", height=" + height
				+ ", designation=" + designation + ", address=" + address + "]";
	}
	private Double weight;
	private Double height;

	private String designation;
	private String address;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	public Double getHeight() {
		return height;
	}
	public void setHeight(Double height) {
		this.height = height;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getUsertype() {
		return usertype;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Long getDoctorSpeciality() {
		return doctorSpeciality;
	}
	public void setDoctorSpeciality(Long doctorSpeciality) {
		this.doctorSpeciality = doctorSpeciality;
	}
	
}
