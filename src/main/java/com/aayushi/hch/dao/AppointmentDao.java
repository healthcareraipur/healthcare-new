package com.aayushi.hch.dao;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.aayushi.hch.entity.Appointment;
import com.aayushi.hch.entity.DoctorSpeciality;
import com.aayushi.hch.entity.Notice;
import com.aayushi.hch.repository.AppointmentRepository;

@Repository
public class AppointmentDao {

	@Autowired
	private AppointmentRepository appointmentRepository;

	public void saveAppointment(Appointment appointment) {
		appointmentRepository.save(appointment);
	}

	public List<Appointment> getAllAppointment(Long id) {

		return appointmentRepository.findByUserIdAndStatusTrue(id);
	}

	public List<Appointment> getByDateAndDoc(Date date, DoctorSpeciality doctorSpeciality) {
		return appointmentRepository.findByAppointmentDateAndDoctorSpecialityAndStatusTrue(date, doctorSpeciality);
	}

	public List<Appointment> getUnsentAppointment(Date date) {
		return appointmentRepository.findByemailSentFalseAndAppointmentDate(date);
	}

	public void updateEmailSent(Long id) {
		Appointment appointment = appointmentRepository.findOne(id);
		if (appointment == null) {
			throw new IllegalArgumentException("appointment doesn't exist with id=" + id);
		}

		appointment.setEmailSent(true);
		appointmentRepository.save(appointment);

	}
	public Appointment getById(Long id) {
		return appointmentRepository.findOne(id);
	}

	public void cancelAppointment(Long id) {
		Appointment appointment = appointmentRepository.findOne(id);
		appointment.setStatus(false);
		appointmentRepository.save(appointment);
	}
	

	public void deleteAppointment(Long id) {
		appointmentRepository.delete(id);
		
	}
	public List<Appointment> findByisVisitedFalseAndAppointmentDate(Date date) {
		return appointmentRepository.findByisVisitedFalseAndAppointmentDateAndStatusTrue(date);
	}

	
}
