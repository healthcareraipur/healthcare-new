package com.aayushi.hch.utility;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.aayushi.hch.dao.AppointmentDao;
import com.aayushi.hch.entity.Appointment;

@Component
public class ReminderTriggerJob {

	@Autowired
	private AppointmentDao appointmentDao;
	@Autowired
	private EmailUtility emailUtility;

	@Scheduled(fixedDelay = 15000)
	public void checkAndSendReminder() throws ParseException {
		Date today = new Date();
		List<Appointment> appointments = appointmentDao.getUnsentAppointment(today);
		for (Appointment appointment : appointments) {
			/*
			 * if (appointment.getReminderDate().before(currdate)) {
			 * emailUtility.sendEmail(reminder.getUser().getEmail(), reminder.getSubject(),
			 * reminder.getDescription()); reminderDao.updateEmailSent(reminder.getId()); }
			 */

			Date currdate = new Date();
			Date startDate = stringToDate(appointment.getDisplaySlot());
			Calendar todaytime = Calendar.getInstance();
			Calendar appDate = Calendar.getInstance();
			todaytime.setTime(currdate);
			appDate.setTime(startDate);
			todaytime.add(Calendar.MINUTE, 30);
			if ((todaytime.get(Calendar.HOUR_OF_DAY) >= appDate.get(Calendar.HOUR_OF_DAY))
					&& ((todaytime.get(Calendar.MINUTE) >= appDate.get(Calendar.MINUTE)))) {
				String subject = "Today's Appointment";
				String description = "You have appointment today for a "
						+ appointment.getDoctorSpeciality().getSpeciality() + " from " + appointment.getDisplaySlot()
						+ " . Kindly be on time.";
				emailUtility.sendEmail(appointment.getUser().getEmail(), subject, description);
				appointmentDao.updateEmailSent(appointment.getAppointmentId());
			}
		}

	}

	private Date stringToDate(String displaySlot) throws ParseException {

		String start = displaySlot.substring(0, 5);
		// String end=s.substring(6, 11);
		DateFormat sdf = new SimpleDateFormat("hh:mm");
		Date startDate = sdf.parse(start);
	//	System.out.println(startDate);
		return startDate;
	}
	private Date stringToEndDate(String displaySlot) throws ParseException {

		//String start = displaySlot.substring(0, 5);
		 String end=displaySlot.substring(6, 11);
		DateFormat sdf = new SimpleDateFormat("hh:mm");
		Date endDate = sdf.parse(end);
	//	System.out.println(startDate);
		return endDate;
	}
	
	@Scheduled(cron="0 0 19 ?  * *")
	public void checkAndSendCancelEmail() throws ParseException {
		Date today = new Date();
		List<Appointment> appointments = appointmentDao.findByisVisitedFalseAndAppointmentDate(today);
		for (Appointment appointment : appointments) {
			String subject = "Appointment Cancelled";
				String description = "Your Appointment has been Cancelled which was during "+ appointment.getDisplaySlot();
				
				emailUtility.sendEmail(appointment.getUser().getEmail(), subject, description);
				appointmentDao.cancelAppointment(appointment.getAppointmentId());
			
		}

	}

}
