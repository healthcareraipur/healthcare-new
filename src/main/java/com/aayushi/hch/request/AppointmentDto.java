package com.aayushi.hch.request;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.aayushi.hch.entity.TimeSlot;

public class AppointmentDto {

	private Long appointment_id;
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date appointment_date;
	private TimeSlot timeSlot;
	private String displaySlot ;
	private boolean emailSent = false;
	
	private boolean status = true;
	private boolean isVisited=false;
	
	
	
	public boolean isVisited() {
		return isVisited;
	}
	public void setVisited(boolean isVisited) {
		this.isVisited = isVisited;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public boolean isEmailSent() {
		return emailSent;
	}
	public void setEmailSent(boolean emailSent) {
		this.emailSent = emailSent;
	}
	public String getDisplaySlot() {
		return displaySlot;
	}
	public void setDisplaySlot(String displaySlot) {
		this.displaySlot = displaySlot;
	}
	
	
	public TimeSlot getTimeSlot() {
		return timeSlot;
	}
	public void setTimeSlot(TimeSlot timeSlot) {
		this.timeSlot = timeSlot;
	}

	private Long userId;
	private Long  doctorSpeciality;
	
	public Date getAppointment_date() {
		return appointment_date;
	}
	public void setAppointment_date(Date appointment_date) {
		this.appointment_date = appointment_date;
	}
	
	public Long getAppointment_id() {
		return appointment_id;
	}
	public void setAppointment_id(Long appointment_id) {
		this.appointment_id = appointment_id;
	}
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getDoctorSpeciality() {
		return doctorSpeciality;
	}
	public void setDoctorSpeciality(Long doctorSpeciality) {
		this.doctorSpeciality = doctorSpeciality;
	}
	
	
	
}
