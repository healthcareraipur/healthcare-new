package com.aayushi.hch.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.aayushi.hch.dao.CommentDao;
import com.aayushi.hch.dao.UserDao;
import com.aayushi.hch.entity.Comment;
import com.aayushi.hch.entity.User;
import com.aayushi.hch.request.CommentDto;

@Controller
public class CommentController {
	
	@Autowired
	CommentDao commentDao;
	
	@Autowired
	UserDao userdao;
	@RequestMapping(method = RequestMethod.GET, value = "/comments")
	public ModelAndView getAllComment(@RequestParam Long userId) {

		List<Comment>comments = new ArrayList<>();
		comments = commentDao.getAllComments();
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("Comment");
		modelAndView.addObject("comments", comments);
		modelAndView.addObject("userId", userId);
		return modelAndView;
	}
	@RequestMapping(method = RequestMethod.POST, value = "/saveComment")
	public ModelAndView saveNotice(@ModelAttribute("commentDto") CommentDto commentDto) {
		commentDto.setCreatedOn(new Date());
		Comment comment = new Comment();
        User user = new User();
        user.setId(commentDto.getCreatedBy());
		BeanUtils.copyProperties(commentDto, comment);
		comment.setCreatedBy(user);
		System.out.println(commentDto+"\n");
		System.out.println(comment);
		commentDao.saveComment(comment);
		ModelAndView modelAndView = new ModelAndView("redirect:" + "/comments");
		modelAndView.addObject("userId", user.getId());
		return modelAndView;
	}

}
