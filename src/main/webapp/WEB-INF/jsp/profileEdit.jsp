<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/Doctor.css"/>
</head>
<body>
<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
		
		</div>
		<ul class="nav navbar-nav">
			
			<li><a href="deleteAccount?userId=${user.id}"  onclick="return confirm('Are you sure you want to delete your account?');">Delete Account</a></li>
		
		</ul >
		
	</div>
	</nav><h3 style="text-align: center"><font color="white">	Edit Profile</font></h3>
	<div class="container" style="top:60%;">

		<form action="/updateProfile" method="post" modelAttribute="userRequestDto">
			<input type="hidden"
					class="form-control" id="id" name="id" value="${user.id}">
			<input type="hidden"
					class="form-control" id="password" name="password" value="${user.password}">
		<input type="hidden"
					class="form-control" id="gender" name="gender" value="${user.gender}">
		
			<input type="hidden"
					class="form-control" id="email" name="email" value="${user.email}">
		<input type="hidden"
					class="form-control" id="usertype" name="usertype" value="${user.usertype}">
		
		
		<input type="hidden"
					class="form-control" id="doctorSpeciality" name="doctorSpeciality" value="${user.doctorSpeciality.id}">
		
		<input type="hidden"
					class="form-control" id="designation" name="designation" value="${user.designation}">
		
			<div class="form-group">
				<label for="fullName">Full Name:</label> <input type="text"
					class="form-control" id="fullName"
					 name="fullName" value="${user.fullName}" required>
			</div>
			
			<div class="form-group">
				<label for="mobile">Mobile:</label> <input type="text"
					class="form-control" id="mobile"
					 name="mobile" value="${user.mobile}" title="e.g : +910000000000 , " pattern="[0-9]{10}"required>
			</div>
			
			<div class="form-group">
				 <input type="hidden"
					class="form-control" id="dob"
					 name="dob" value="${user.dob} "required>
			</div>
		


			
<div class="form-group">
				<label for="weight">weight:</label> <input type="number" step="0.01"
					class="form-control" id="weight"
					name="weight" value="${user.weight}" min="0"  required>
			</div>
			<div class="form-group">
				<label for="height">height:</label> <input type="number" step="0.01"
					class="form-control" id="height" min="0" 
					name="height" value="${user.height}"required>
			</div>
			<div class="form-group">
				<label for="address">Permanent Address:</label> <input type="text"
					class="form-control" id="address"
					 name="address" value="${user.address}"required>
			</div>
			
			
			
			
			
			<button type="submit" class="btn btn-default" value="updateProfile">Submit</button>
			<a href="http://localhost:8080/getEmail" style="color: #fff">Change
				password</a>
		</form>
	</div>

</body>
</html>