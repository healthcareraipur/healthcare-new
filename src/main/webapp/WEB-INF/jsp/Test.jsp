<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="css/Doctor.css"/>
<title>Insert title here</title>
</head>
<body>
<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			
		</div>
		<h3 style="text-align: center"><font color="black">	Test Report</font></h3>
	</div>
	</nav>
	<div class="container" style="width:40%; top:130%; ">

		<form action="saveReport" method="post" modelAttribute="reportDto">
		<div class="form-group">
		<input type="hidden"
        					class="form-control" id="userId" name="userId" value="${user.id}">

        					<b>Full Name :<em> ${user.fullName}</em></b> <br><hr>
                            <b>Email : <em>${user.email}</em></b><br><hr>
                            <b>Date of Birth : <em>${user.dob}</em></b><br><hr>
                            <b>Mobile Number : <em>${user.mobile}</em></b><br><hr>
                            <b>Gender :<em> ${user.gender}</em></b><br><hr>
                            <b>Weight : <em>${user.weight}</em></b><br><hr>
                            <b>Height :<em> ${user.height}</em></b><br><hr>
                            <b>Address :<em> ${user.address}</em></b>


			<div class="form-group">
				<label for="date">Report Date:</label> <input type="date"
					class="form-control" id="date"
					name="date" min=' '>
			</div>
			
		<div class="form-group">
        				<label for="bloodGroup">bloodGroup:</label> <input type="text"
        					class="form-control"  id="bloodGroup"
        					name="bloodGroup"required>
        			</div>

        <div class="form-group">
                <label for="hemoglobin">hemoglobin:</label> <input type="number" step="0.01"
                    	class="form-control" id="hemoglobin"
                    	name="hemoglobin"required  min="0">
        </div>
        <div class="form-group">
                        <label for="rbcCount">rbcCount:</label> <input type="number" step="0.01"
                            	class="form-control" id="rbcCount"
                            	name="rbcCount"required  min="0">
        </div>

        <div class="form-group">
                                <label for="diaBP">diaBP:</label> <input type="number"
                                    	class="form-control" id="diaBP"
                                    	name="diaBP"required  min="0">
                </div>
                <div class="form-group">
                                        <label for="sysBP">sysBP:</label> <input type="number"
                                            	class="form-control" id="sysBP"
                                            	name="sysBP"required  min="0">
        </div>
         <div class="form-group">
         <label for="fastingSugar">fastingSugar:</label> <input type="number"
         class="form-control" id="fastingSugar" name="fastingSugar"required  min="0">
         </div>

         <div class="form-group">
                  <label for="afterMealSugar">afterMealSugar:</label> <input type="number"
                  class="form-control" id="afterMealSugar" name="afterMealSugar"required  min="0">
                  </div>

         <div class="form-group">
              <label for="platelet">platelet:</label> <input type="number"
                class="form-control" id="platelet" name="platelet"required  min="0">
          </div>
         <div class="form-group">
                                  <label for="rbcCount">rbcCount:</label> <input type="number" step="0.01"
                                      	class="form-control" id="rbcCount"
                                      	name="rbcCount"required  min="0">
          </div>

         <div class="form-group">
          <label for="calcium">calcium:</label> <input type="number" step="0.01"
           class="form-control" id="calcium" name="calcium"required  min="0">
           </div>

         <div class="form-group">
          <label for="thyroid_T3">rbcCount:</label> <input type="number" step="0.01"
           class="form-control" id="thyroid_T3" name="thyroid_T3" required  min="0">
           </div>
           <div class="form-group">
            <label for="thyroid_T4">thyroid_T4:</label> <input type="number" step="0.01"
              class="form-control" id="thyroid_T4" name="thyroid_T4" required  min="0">
              </div>
  
</select>
<br>
<br>
			

			<button type="submit"  value="saveReport">Submit</button>
		</form>
	</div>



</body>

</html>