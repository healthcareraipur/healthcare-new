package com.aayushi.hch.utility;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.stereotype.Component;

@Component
public class EmailUtility {

	private static String USER_NAME = "healthcareraipur@gmail.com"; 
	private static String PASSWORD = "healthcare29"; // GMail password

	public void sendEmail(String toEmail, String subject, String description) {
		String from = USER_NAME;
		String pass = PASSWORD;
		String[] to = { toEmail }; // list of recipient email addresses
		// String subject = "Java send mail example - 2";
		// String body = "<h1>Welcome to JavaMail -2 !<h1>";

		Properties props = System.getProperties();
		// String host = "smtp.gmail.com";
		props.setProperty("mail.transport.protocol", "smtp");
		props.setProperty("mail.host", "smtp.gmail.com");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.auth", "true");

		Session session = Session.getDefaultInstance(props);
		MimeMessage message = new MimeMessage(session);

		try {
			message.setFrom(new InternetAddress(from));
			InternetAddress[] toAddress = new InternetAddress[to.length];

			// To get the array of addresses
			for (int i = 0; i < to.length; i++) {
				toAddress[i] = new InternetAddress(to[i]);
			}

			for (int i = 0; i < toAddress.length; i++) {
				message.addRecipient(Message.RecipientType.TO, toAddress[i]);
			}

			message.setSubject(subject);
			message.setContent(description, "text/html");

			Transport transport = session.getTransport("smtp");
			transport.connect("smtp.gmail.com", 587, from, pass);
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();
			System.out.println("sent email with Subject " + subject + " , to email " + toEmail);
		} catch (AddressException ae) {
			ae.printStackTrace();
		} catch (MessagingException me) {
			me.printStackTrace();
		}
	}

	public void sendPdf(String toEmail,String pdfPath) {
		Properties props = System.getProperties();
		// String host = "smtp.gmail.com";
		props.setProperty("mail.transport.protocol", "smtp");
		props.setProperty("mail.host", "smtp.gmail.com");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.auth", "true");

		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {

			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				// TODO Auto-generated method stub
				return new PasswordAuthentication(USER_NAME, PASSWORD);
			}

		});
		MimeMessage message = new MimeMessage(session);
		try {
			message.setFrom(new InternetAddress(USER_NAME));

			message.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));

			message.setSubject("Report File is hERE !!");
			// Create the message part
			BodyPart messageBodyPart = new MimeBodyPart();

			// Fill the message
			messageBodyPart.setText("Your Test Report From HealthCare Portal");

			// Create a multipar message
			Multipart multipart = new MimeMultipart();

			// Set text message part
			multipart.addBodyPart(messageBodyPart);

			// Part two is attachment
			messageBodyPart = new MimeBodyPart();
			//String filename = "e:\\BloodReport.pdf";
			DataSource source = new FileDataSource(pdfPath);
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(pdfPath);
			multipart.addBodyPart(messageBodyPart);

			// Send the complete message parts
			message.setContent(multipart);
			messageBodyPart.setText("We Wish You a Good Health.");
			messageBodyPart.setText("-HealthCare Portal ,Raipur");
			// Send message
			Transport.send(message);
			System.out.println("Sent message successfully....");
		} catch (MessagingException mex) {
			mex.printStackTrace();
		}
	}

}
