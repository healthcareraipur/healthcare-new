package com.aayushi.hch.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.aayushi.hch.entity.Comment;

public interface CommentRepository extends JpaRepository<Comment, Long>{

	public List<Comment> findAllByOrderByCreatedOnDesc();
}
