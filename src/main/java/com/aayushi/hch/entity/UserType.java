package com.aayushi.hch.entity;

public enum UserType {
	PATIENT, DOCTOR, ADMIN;
}
