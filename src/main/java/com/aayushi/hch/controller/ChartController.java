package com.aayushi.hch.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.aayushi.hch.dao.ReportDao;
import com.aayushi.hch.entity.Report;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

@Controller
public class ChartController {
	@Autowired
	ReportDao reportDao;

	@RequestMapping(method = RequestMethod.GET, value = "/healthChart")
	public ModelAndView getAllChart(@RequestParam Long userId) throws JsonProcessingException {

		List<Report> reports = new ArrayList<>();
		reports = reportDao.getById(userId);
		ObjectMapper objectMapper = new ObjectMapper();
    	
    	objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
    	String reportJson = objectMapper.writeValueAsString(reports);
    	System.out.println(reportJson);
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("Chart");
		modelAndView.addObject("reports", reportJson);
		return modelAndView;
	}
}
