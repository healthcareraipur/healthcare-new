package com.aayushi.hch.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "doctor_speciality")
public class DoctorSpeciality {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String speciality;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSpeciality() {
		return speciality;
	}

	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}

	@Override
	public String toString() {
		return "DoctorSpeciality [id=" + id + ", speciality=" + speciality + "]";
	}

}
