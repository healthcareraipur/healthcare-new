package com.aayushi.hch.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.aayushi.hch.entity.Notice;

public interface NoticeRepository extends JpaRepository<Notice, Long>{

}
