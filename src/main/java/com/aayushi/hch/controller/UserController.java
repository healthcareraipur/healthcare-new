package com.aayushi.hch.controller;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.aayushi.hch.dao.UserDao;
import com.aayushi.hch.entity.DoctorSpeciality;
import com.aayushi.hch.entity.Gender;
import com.aayushi.hch.entity.User;
import com.aayushi.hch.entity.UserType;
import com.aayushi.hch.request.UserRequestDto;

@Controller
public class UserController {

	@Autowired
	private UserDao userDao;

	// brings the Register.jsp page
	@RequestMapping(method = RequestMethod.GET, value = "/register")
	public ModelAndView getRegister() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("Register");
		return modelAndView;

	}

	// user details are saved in DB and login page is rendered
	@RequestMapping(method = RequestMethod.POST, value = "/register")
	public ModelAndView addUser(@ModelAttribute("userRequestDto") UserRequestDto userRequestDto) {
		User user = new User();
		//System.out.println(userRequestDto);
		BeanUtils.copyProperties(userRequestDto, user);
		addExtraInfo(user, userRequestDto);

		userDao.saveUser(user);
		return new ModelAndView("redirect:" + "/loginHere");
	}
   // changes the string type to enum type
	private void addExtraInfo(User user, UserRequestDto userRequestDto) {
		user.setGender(Gender.valueOf(userRequestDto.getGender()));
		user.setUsertype(UserType.valueOf(userRequestDto.getUsertype()));
		DoctorSpeciality doctorSpeciality = new DoctorSpeciality();
		doctorSpeciality.setId(userRequestDto.getDoctorSpeciality());
		user.setDoctorSpeciality(doctorSpeciality);

	}
    // brings the login.jsp page
	@RequestMapping(method = RequestMethod.GET, value = "/loginHere")
	public ModelAndView getLogin() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("login");
        return modelAndView;
	}
	@RequestMapping(method = RequestMethod.GET, value = "/MainPage")
	public ModelAndView getMainPage() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("MainPage");
        return modelAndView;
	}
	@RequestMapping(method = RequestMethod.GET, value = "/FAQ")
	public ModelAndView getFAQ() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("FAQ");
        return modelAndView;
	}
	
	
    //authenticate for the valid user
	@RequestMapping(method = RequestMethod.POST, value = "/login")
	public ModelAndView checkUser(@ModelAttribute("user") User user) {
		User userDb = userDao.findByEmailAndPassword(user.getEmail(), user.getPassword());
		if (userDb == null) {
			return new ModelAndView("redirect:" + "/loginHere");
		} else {
			if(userDb.getUsertype()==UserType.PATIENT) {
			ModelAndView modelAndView = new ModelAndView("redirect:" + "/allAppointment");
	//		System.out.println();
			modelAndView.addObject("userId", userDb.getId());
			return modelAndView;
			
			}
			else if(userDb.getUsertype()==UserType.DOCTOR) {
				ModelAndView modelAndView = new ModelAndView("redirect:" + "/DoctorAppointment");
						modelAndView.addObject("userId", userDb.getId());
						return modelAndView;
			}
			
			else {
				ModelAndView modelAndView = new ModelAndView("redirect:" + "/allNotice");
				modelAndView.addObject("userId", userDb.getId());
				return modelAndView;
			}
		}

	}
	@RequestMapping(method = RequestMethod.GET, value = "/profileEdit")
	public ModelAndView getprofileEdit(@RequestParam Long userId) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("profileEdit");
		User user = userDao.getById(userId);
		modelAndView.addObject("user", user);
		return modelAndView;

	}
	@RequestMapping(method = RequestMethod.POST, value = "/updateProfile")
	public ModelAndView updateProfile(@ModelAttribute("userRequestDto") UserRequestDto userRequestDto) {
		User user = new User();
		BeanUtils.copyProperties(userRequestDto, user);
		addExtraInfo(user, userRequestDto);
		userDao.updateUser(user, user.getId());
		if(user.getUsertype()==UserType.DOCTOR) {
			ModelAndView modelAndView = new ModelAndView("redirect:" + "/DoctorAppointment");
			modelAndView.addObject("userId", user.getId());
			return modelAndView;
		}
		else {
		ModelAndView modelAndView = new ModelAndView("redirect:" + "/allAppointment");
		modelAndView.addObject("userId", user.getId());
		return modelAndView;
		}
	}
	@RequestMapping(method = RequestMethod.GET, value = "/deleteAccount")
	public ModelAndView deleteAccount(@RequestParam Long userId) {
		userDao.deleteUser(userId);
		return new ModelAndView("redirect:" + "/loginHere");
	}
	@RequestMapping(method = RequestMethod.GET, value = "/getPatientList")
	public ModelAndView getPatientList() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("PatientList");
		List<User> users = userDao.getAllUser();
		modelAndView.addObject("users", users);
		return modelAndView;

	}
}
