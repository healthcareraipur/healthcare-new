package com.aayushi.hch.entity;

public enum TimeSlot {

	SLOT1("10:00-10:30"),

	SLOT2("10:30-11:00"),

	SLOT3("11:00-11:30"),

	SLOT4("11:30-12:00"),
	SLOT5("12:00-12:30"), 
	SLOT6("12:30-13:00"), 
	SLOT7("13:00-13:30"), 
	SLOT8("13:30-14:00"), 
	SLOT9("15:00-15:30"),
	SLOT10("15:30-16:00"), 
	SLOT11("16:00-16:30"), 
	SLOT12("16:30-17:00"), 
	SLOT13("17:00-17:30"),
	SLOT14("17:30-18:00"), 
	SLOT15("18:00-18:30"), 
	SLOT16("18:30-19:00");

	private String displayValue;

	private TimeSlot(String displayValue) {
		this.displayValue = displayValue;
	}

	public String getDisplayVaule() {
		return this.displayValue;
	}

	public static TimeSlot fromDisplayValue(String displayVaule) {
		for (TimeSlot timeSlot : TimeSlot.values()) {
			if (timeSlot.getDisplayVaule().equals(displayVaule)) {
				return timeSlot;
			}
		}
		return null;
	}
	
    public String toString() {
        return displayValue;
    }
}
