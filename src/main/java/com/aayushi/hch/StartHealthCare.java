package com.aayushi.hch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@ComponentScan
public class StartHealthCare {
public static void main(String[] args) {
	SpringApplication.run(StartHealthCare.class, args);
}
}
