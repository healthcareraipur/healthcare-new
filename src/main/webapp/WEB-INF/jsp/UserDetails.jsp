<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="css/Doctor.css"/>
<title>Insert title here</title>
</head>
<body>
<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			
		</div>
		<ul class="nav navbar-nav"></ul >
		</div>
	</nav>
	<h2><font color="lightblue"><center>Patient Details</center></font></h2> 
	
	<div class="container"style="top:70%; ">
	
<b>Full Name :<em> ${appointment.user.fullName}</em></b> <br><hr>
<b>Email : <em>${appointment.user.email}</em></b><br><hr>
<b>Date of Birth : <em>${appointment.user.dob}</em></b><br><hr>
<b>Mobile Number : <em>${appointment.user.mobile}</em></b><br><hr>
<b>Gender :<em> ${appointment.user.gender}</em></b><br><hr>
<b>Weight : <em>${appointment.user.weight}</em></b><br><hr>
<b>Height :<em> ${appointment.user.height}</em></b><br><hr>
<b>Address :<em> ${appointment.user.address}</em></b>
<form action="visited" method="post" modelAttribute="appointmentId" modelAttribute="isVisited" modelAttribute="userId" > 
<input type="hidden"
					class="form-control" id="appointmentId" value="${appointment.appointmentId}" name="appointmentId">
<input type="hidden"
					class="form-control" id="userId" value="${userId}" name="userId">
<hr><b>Is Visited:</b>

<select name="isVisited">
  <option value="true">Yes</option>
  <option value="false">NO</option>
  
  
</select>

<button type="submit"  value="visited"><b><em>Submit</em></button>
</form>
</div>
</body>

</html>