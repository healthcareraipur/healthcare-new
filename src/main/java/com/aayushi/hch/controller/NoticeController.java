package com.aayushi.hch.controller;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.aayushi.hch.dao.NoticeDao;
import com.aayushi.hch.entity.Notice;
import com.aayushi.hch.request.NoticeDto;

@Controller
public class NoticeController {

	@Autowired
	private NoticeDao noticeDao;

	@RequestMapping(method = RequestMethod.POST, value = "/saveNotice")
	public ModelAndView saveNotice(@ModelAttribute("noticeDto") NoticeDto noticeDto) {
		Notice notice = new Notice();

		BeanUtils.copyProperties(noticeDto, notice);
		noticeDao.saveNotice(notice);
		return new ModelAndView("redirect:" + "/allNotice");
	}

	@RequestMapping(method = RequestMethod.GET, value = "/newNotice")
	public ModelAndView getRegister() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("AddNotice");
		return modelAndView;

	}

	@RequestMapping(method = RequestMethod.GET, value = "/allNotice")

	public ModelAndView getAllNotice(@RequestParam Long userId) {
       List<Notice> allNotice = noticeDao.getAllNotices();
       ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("AllNotice");
		modelAndView.addObject("allNotice", allNotice);
		modelAndView.addObject("userId", userId);
		return modelAndView;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/NoticeDetails")
	public ModelAndView getNoticeDetails(@RequestParam Long noticeId) {
		Notice notice = noticeDao.getById(noticeId);
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("NoticeDetails");
		modelAndView.addObject("notice", notice);
		return modelAndView;

	}

	@RequestMapping(method = RequestMethod.GET, value = "/updateNotice")
	public ModelAndView updateNotice(@RequestParam Long id) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("UpdateNotice");

		Notice notice = noticeDao.getById(id);
		modelAndView.addObject("notice", notice);

		return modelAndView;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/update")
	public ModelAndView update(@ModelAttribute("noticeDto") NoticeDto noticeDto) {
		Notice notice = new Notice();
		BeanUtils.copyProperties(noticeDto, notice);
		noticeDao.updateNotice(notice, notice.getId());
		return new ModelAndView("redirect:" + "/allNotice");
	}

	@RequestMapping(method = RequestMethod.GET, value = "/delete")
	public ModelAndView deleteReminder(@RequestParam Long id) {
		Notice notice = noticeDao.getById(id);
		if (notice == null) {
			throw new IllegalArgumentException("Invalid notice id : " + id);
		}
		noticeDao.deleteNotice(id);
		return new ModelAndView("redirect:" + "/allNotice");
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/UserNotice")

	public ModelAndView getUserNotice() {

		List<Notice> allNotice = noticeDao.getAllNotices();

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("UserNotice");
		modelAndView.addObject("allNotice", allNotice);

		return modelAndView;
	}
}
