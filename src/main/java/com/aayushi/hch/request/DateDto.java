package com.aayushi.hch.request;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class DateDto {

	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date appointment_date;

	public Date getAppointment_date() {
		return appointment_date;
	}

	public void setAppointment_date(Date appointment_date) {
		this.appointment_date = appointment_date;
	}
	
	
}
