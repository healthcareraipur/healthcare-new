<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>


<script type="text/javascript">
function logout() {
var answer = confirm ("you have successfully logged off , click on OK to continue.")
if (answer)
window.location="http://localhost:8080/loginHere";
}
</script>
<!-- javascript:AlertIt(); -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="css/Comment.css"/>
<title>Insert title here</title>
</head>
<body>
	<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			
		</div>
		<ul class="nav navbar-nav">
			<li class="active"><a href="#">Home</a></li>
			<li><a href="appointment?userId=${userId}">New Appointment</a></li>
			<li><a href="UserNotice">Notices</a></li>
			<li><a href="profileEdit?userId=${userId}">Edit Profile</a></li>
		
		</ul >
		
		<h4 style="text-align: right"><a href="javascript:logout();" style ="color : black">Logout</a></h4>
		
	</div>
	</nav>
	  <h3 class="header1" ><center><font color="lightblue">Question</font></center></h3><hr>
 <div><center>
           <b>Created By:</b> <font color="blue"> 	${comment.createdBy.fullName}<br> </font>
           
    <b>Topic:</b>${comment.topic}<br>
	<b>Description:</b> ${comment.description}
   <font color="grey"> <h6> <b>Created On:</b>	${comment.createdOn}
	<b>Modified On:</b>${comment.modifiedOn}</h6></font>
	<hr>
	</font></center>
	</div>	
         <h3 class="header2" ><center><font color="blue">Replies</font></center></h3><hr>
           
           <div class="container2" >
  <center><font color="white" size="5px"> Reply</font></center>
		<form action="saveReply" method="post" modelAttribute="replyDto">

			<input type="hidden"
					class="form-control" id="createdBy" value="${userId}" name="createdBy">
			<input type="hidden"
					class="form-control" id="commentId" value="${comment.commentId}" name="commentId">
			
			<div class="form-group">
			<label for="description"><font color ="white">Description:</font></label> <input type="text"
					class="form-control" id="description"
					placeholder="Enter description" name="description"required>
			</div>

			<center><button type="submit" value="saveReply"><b>Submit</b></button></center>
		</form>
	</div>
 
	<div class="replycontainer" >

		 <h3 class="header1" ><center><font color="grey">Replies</font></center></h3><hr>
				<c:forEach var="replies" items="${replies}">
			<font color="blue"> 		<b>	${replies.createdBy.fullName}<br></b></font>
					<b>Description:</b> ${replies.description}<br>
					    <h6><a href="deleteAppointment?id=${appointment.appointmentId}"  onclick="return confirm('Are you sure you want to delete this item?');"><div class="glyphicon glyphicon-trash"></div></a>
					  <a href="deleteAppointment?id=${appointment.appointmentId}"  onclick="return confirm('Are you sure you want to delete this item?');"><div class="glyphicon glyphicon-pencil"></div></a>
		    <font color="grey"> <b>Created On:</b>	${replies.createdOn}
					<b>Modified On:</b>${replies.modifiedOn}<br></h6></font>
			
					<br><br>

				</c:forEach>

	</div>
           
	
</body>
</html>