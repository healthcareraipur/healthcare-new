package com.aayushi.hch.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.aayushi.hch.entity.Comment;
import com.aayushi.hch.entity.Reply;
import com.aayushi.hch.repository.ReplyRepository;

@Repository
public class ReplyDao {

	@Autowired
	ReplyRepository replyRepository;
	
	public void saveReply(Reply reply) {
		replyRepository.save(reply);
	}
	public List<Reply> getAllReply(Comment commentId){
		return replyRepository.findByCommentIdOrderByCreatedOnDesc(commentId);
	}
	
}
