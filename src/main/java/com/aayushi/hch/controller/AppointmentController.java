package com.aayushi.hch.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.aayushi.hch.dao.AppointmentDao;
import com.aayushi.hch.entity.Appointment;
import com.aayushi.hch.entity.DoctorSpeciality;
import com.aayushi.hch.entity.Notice;
import com.aayushi.hch.entity.TimeSlot;
import com.aayushi.hch.entity.User;
import com.aayushi.hch.request.AppointmentDto;
import com.aayushi.hch.request.NoticeDto;
import com.aayushi.hch.utility.EmailUtility;

@Controller
public class AppointmentController {
	@Autowired
	private AppointmentDao appointmentDao;
	@Autowired
	private EmailUtility emailUtility;

	@RequestMapping(method = RequestMethod.GET, value = "/appointment")
	public ModelAndView getAppointment(@RequestParam Long userId) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("Appointment");
		modelAndView.addObject("userId", userId);
		return modelAndView;

	}

	@RequestMapping(method = RequestMethod.POST, value = "/checkSlot")
	public ModelAndView checkSlot(@ModelAttribute("appointmentDto") AppointmentDto appointmentDto) {
		DoctorSpeciality doctorSpeciality = new DoctorSpeciality();
		doctorSpeciality.setId(appointmentDto.getDoctorSpeciality());
		List<Appointment> appointments = appointmentDao.getByDateAndDoc(appointmentDto.getAppointment_date(),
				doctorSpeciality);
		Map<TimeSlot, String> timeSlots = null;
		if (appointments != null) {
			timeSlots = getTimeSlotAvailable(appointments);

		}
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String strDate = formatter.format(appointmentDto.getAppointment_date());
		System.out.println(strDate);
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("checkSlot");
		modelAndView.addObject("timeSlots", timeSlots);
		modelAndView.addObject("appointmentDto", appointmentDto);
		modelAndView.addObject("strDate", strDate);
		System.out.println(appointmentDto);
		return modelAndView;

	}

	public Map<TimeSlot, String> getTimeSlotAvailable(List<Appointment> appointments) {

		List<TimeSlot> timeSlots = new ArrayList<>(Arrays.asList(TimeSlot.values()));

		for (Appointment appointment : appointments) {
			if (timeSlots.contains(appointment.getTimeSlot())) {
				timeSlots.remove(appointment.getTimeSlot());
			}
		}

		Map<TimeSlot, String> map = new LinkedHashMap<>();
		for (TimeSlot timeSlot : timeSlots) {
			map.put(timeSlot, timeSlot.getDisplayVaule());
		}
		return map;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/saveSlot")
	public ModelAndView addAppointment(@ModelAttribute("appointmentDto") AppointmentDto appointmentDto,
			@ModelAttribute("date") String date) {

		Appointment appointment = new Appointment();
		BeanUtils.copyProperties(appointmentDto, appointment);
		System.out.println("-------------------------");
		// System.out.println("hello"+ date);
		System.out.println(appointmentDto);
		User user = new User();
		DoctorSpeciality doctorSpeciality = new DoctorSpeciality();
		user.setId(appointmentDto.getUserId());
		doctorSpeciality.setId(appointmentDto.getDoctorSpeciality());
		appointment.setUser(user);
		appointment.setDoctorSpeciality(doctorSpeciality);

		try {
			appointment.setAppointmentDate(new SimpleDateFormat("yyyy-MM-dd").parse(date));
		} catch (ParseException e) {

			e.printStackTrace();
		}
		appointment.setDisplaySlot(appointmentDto.getTimeSlot().getDisplayVaule());
		appointment.setTimeSlot(TimeSlot.fromDisplayValue(appointment.getDisplaySlot()));
		System.out.println(appointment);
		appointmentDao.saveAppointment(appointment);
		ModelAndView modelAndView = new ModelAndView("redirect:" + "/allAppointment");
		modelAndView.addObject("userId", user.getId());
		return modelAndView;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/allAppointment")
	// userId is fetched from the login
	public ModelAndView getAllAppointment(@ModelAttribute("userId") Long userId) {
		Long Id = userId;
		Date date = new Date();
		Calendar today = Calendar.getInstance();
		Calendar appDate = Calendar.getInstance();
		today.setTime(date);

		List<Appointment> allAppointment = appointmentDao.getAllAppointment(Id);
		List<Appointment> upcoming = new ArrayList<>();
		for (Appointment appointment : allAppointment) {
			appDate.setTime(appointment.getAppointmentDate());
			if ((appDate.get(Calendar.MONTH) >= (today.get(Calendar.MONTH))
					&& (appDate.get(Calendar.DAY_OF_MONTH) >= today.get(Calendar.DAY_OF_MONTH)))) {
				upcoming.add(appointment);

			}
		}

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("allAppointment");
		modelAndView.addObject("allAppointment", upcoming);

		if (!allAppointment.isEmpty()) {
			modelAndView.addObject("userId", allAppointment.get(0).getUser().getId());
		}
		return modelAndView;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/cancel")
	public ModelAndView cancelAppointment(@RequestParam Long id) {
		Appointment appointment = appointmentDao.getById(id);
		if (appointment == null) {
			throw new IllegalArgumentException("Invalid appointment id : " + id);
		}
		appointmentDao.cancelAppointment(id);
		String subject = "Appointment Cancelled";
		String description = "Your Appointment has been Cancelled which was during "+ appointment.getDisplaySlot();
		emailUtility.sendEmail(appointment.getUser().getEmail(), subject, description);
		ModelAndView modelAndView = new ModelAndView("redirect:" + "/allAppointment");
		modelAndView.addObject("userId", appointment.getUser().getId());
		return modelAndView;
	}

	

	

	@RequestMapping(method = RequestMethod.GET, value = "/deleteAppointment")
	public ModelAndView deleteAppointment(@RequestParam Long id) {
		Appointment appointment = appointmentDao.getById(id);
		if (appointment == null) {
			throw new IllegalArgumentException("Invalid appointment id : " + id);
		}
		appointmentDao.deleteAppointment(id);
		ModelAndView modelAndView = new ModelAndView("redirect:" + "/allAppointment");
		modelAndView.addObject("userId", appointment.getUser().getId());
		return modelAndView;
	}
}
