package com.aayushi.hch.controller;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.aayushi.hch.dao.ReportDao;
import com.aayushi.hch.dao.UserDao;
import com.aayushi.hch.entity.Report;
import com.aayushi.hch.entity.User;
import com.aayushi.hch.request.ReportDto;
import com.aayushi.hch.utility.EmailUtility;
import com.aayushi.hch.utility.PDFUtility;

@Controller
public class ReportController {
	@Autowired
	private ReportDao reportDao;

	@Autowired
	private UserDao userdao;

	@Autowired
	private PDFUtility pdfUtility;

	@Autowired
	EmailUtility emailUtility;

	@RequestMapping(method = RequestMethod.GET, value = "/testReport")
	public ModelAndView getAppointment(@RequestParam Long userId) {
		ModelAndView modelAndView = new ModelAndView();
		User user = userdao.getById(userId);
		modelAndView.setViewName("Test");
		modelAndView.addObject("user", user);
		return modelAndView;

	}

	@RequestMapping(method = RequestMethod.POST, value = "/saveReport")
	public ModelAndView saveReport(@ModelAttribute("reportDto") ReportDto reportDto) {
		Report report = new Report();
		// System.out.println(userRequestDto);
		BeanUtils.copyProperties(reportDto, report);
		report.setUser(userdao.getById(reportDto.getUserId()));
		reportDao.saveReport(report);
		String pdfPath = "e:/" + report.getUser().getFullName() + ".pdf";
		pdfUtility.generatePdf(report, pdfPath);
		emailUtility.sendPdf(report.getUser().getEmail(), pdfPath);
		return new ModelAndView("redirect:" + "/loginHere");
	}

}
