<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/Register.css"/>

</head>
<body>

<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			
		</div>
		<h3 style="text-align: left"><font color="#d7f5ff"><em><b><span class="highlight" font-color="black">Patient Satisfactory & Informative Government HealthCare Web Portal</span>  </em>	</font></h3>
	</div>
	</nav>
		<h3 style="text-align:center "><font color="black"><em><span class="highlight"> Registration</span>  </em>	</font></h3>
	
	<div class="container">

		<form action="register" method="post" modelAttribute="userRequestDto">

			<div class="form-group">
				<label for="fullName">Full Name:</label> <input type="text"
					class="form-control" id="fullName"
					placeholder="Enter your full Name"   name="fullName" required>
			</div>
			<div class="form-group">
				<label for="email">Email:</label> <input type="text"
					class="form-control" pattern=".+@.+.com" title="e.g : xyz@abc.com" id="email" placeholder="Enter email"
					name="email"required>
			</div>
			<div class="form-group">
				<label for="password">Password:</label> <input type="password"
					class="form-control" id="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" name="password" required>
			</div>
		<!-- <div class="form-group">
				<label for="usertype">usertype:</label> <input type="text"
					class="form-control" id="usertype" name="usertype">
			</div> -->	
			<br>
			<div class="form-group" >
	User Type:	<div class="custom-select" style="width:200px;">	<select name="usertype" id="usertype">
  <option value="PATIENT">Patient</option>
  <option value="DOCTOR">Doctor</option>
 
  
</select></div></div>
	<br>		

<div class="form-group">
				<label for="dob">Date of Birth:</label> <input type="date"
					class="form-control" id="dob"
					 name="dob" required>
			</div>
			
			<div class="form-group">
				<label for="mobile">Mobile:</label> <input type="text"
					class="form-control" id="mobile"
					placeholder="Enter mobile" name="mobile" title="e.g : +910000000000 , " pattern="[0-9]{10}" required>
			</div>
			<!-- <div class="form-group">
				<label for="gender">gender:</label> <input type="text"
					class="form-control" id="gender" name="gender">
			</div>
			
			<div class="form-group">
				<label for="doctorSpeciality">doctorSpeciality:</label> <input type="number"
					class="form-control" id="doctorSpeciality" name="doctorSpeciality">
			</div> -->
			<br>
		<div class="form-group">Gender:	
		
			<div class="custom-select" style="width:200px;">
		<select name="gender">
  <option value="MALE">MALE</option>
  <option value="FEMALE">FEMALE</option>
  
  
</select></div><br><br>

<div class="form-group" > Specialist:<div class="custom-select" style="width:200px;">
		<select name="doctorSpeciality" id="doctype">
  <option value="1">child specialist</option>
  <option value="2">neurologist</option>
   <option value="3">heart specialist</option>
   <option value="4">NONE</option>
  
</select>
</div>
</div>
<br>
<br>
			
<div class="form-group">
				<label for="weight">weight:</label> <input type="number" step="0.01"
					class="form-control" id="weight" placeholder="Enter your weight(in kg)"
					name="weight"required  min="0">
			</div>
			<div class="form-group">
				<label for="height">height:</label> <input type="number" step="0.01"
					class="form-control" id="height" placeholder="Enter your height "
					name="height" min="0" required>
			</div>
			<div class="form-group">
				<label for="address">Permanent Address:</label> <input type="text"
					class="form-control" id="address"
					placeholder="Enter your address" name="address"required>
			</div>
			
			<div class="form-group">
				<label for="address">designation:</label> 
				
			<div class="custom-select" style="width:200px;">
					<select name="designation" id="design">
  <option value="MBBS">MBBS</option>
  <option value="MD">MD</option>
   <option value="NONE">NONE</option>
   
  
</select>
</div>
			</div>
			
			
			
			
			
			
			

			
			
			
			


			




			<button type="submit"  value="register" onclick=""><b><em>Submit</em></button>
		</form>
	</div>

</body>
<script type="text/javascript">
           function Validate()
            {
                var e = document.getElementById("usertype");
                var strUser = e.options[e.selectedIndex].value;
                var e1 = document.getElementById("doctype");
                var strUser1 = e1.options[e1.selectedIndex].value;
                var e2 = document.getElementById("design");
                var strUser2 = e1.options[e1.selectedIndex].value;
                if(strUser=="PATIENT" && strUser1!="NONE" && strUser2!="NONE" )
                {
                     var a=confirm("Please select a None in Doctor's speciality and designation");
                     if (a)
                    	 window.location="http://localhost:8080/register";
                   
                }
            }
        </script>
<script src="js/customdropdown.js"></script>
</html>