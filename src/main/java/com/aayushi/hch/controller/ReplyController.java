package com.aayushi.hch.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.aayushi.hch.dao.CommentDao;
import com.aayushi.hch.dao.ReplyDao;
import com.aayushi.hch.dao.UserDao;
import com.aayushi.hch.entity.Comment;
import com.aayushi.hch.entity.Reply;
import com.aayushi.hch.entity.User;
import com.aayushi.hch.request.ReplyDto;

@Controller
public class ReplyController {
	@Autowired
	UserDao userdao;
	
	@Autowired
	CommentDao commentDao;
	
	@Autowired
	ReplyDao replyDao;

	@RequestMapping(method = RequestMethod.GET, value = "/replies")
	public ModelAndView getAllAppointment(@RequestParam Long commentId, @RequestParam Long userId) {
		//User user = userdao.getById(userId);
		Comment comment = commentDao.getById(commentId);

		List<Reply>replies = new ArrayList<>();
		replies = replyDao.getAllReply(comment);
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("Reply");
		modelAndView.addObject("comment", comment);
		modelAndView.addObject("userId", userId);
		modelAndView.addObject("replies", replies);
		return modelAndView;
	}
	@RequestMapping(method = RequestMethod.POST, value = "/saveReply")
	public ModelAndView saveNotice(@ModelAttribute("replyDto") ReplyDto replyDto) {
		replyDto.setCreatedOn(new Date());
		Comment comment = new Comment();
		Reply reply = new Reply();
        User user = new User();
        comment.setCommentId(replyDto.getCommentId());
        user.setId(replyDto.getCreatedBy());
		BeanUtils.copyProperties(replyDto, reply);
		reply.setCreatedBy(user);
		reply.setCommentId(comment);
		replyDao.saveReply(reply);
		ModelAndView modelAndView = new ModelAndView("redirect:"+"/replies");
		modelAndView.addObject("commentId", comment.getCommentId());
		modelAndView.addObject("userId", user.getId());
		return modelAndView;
	}
}
