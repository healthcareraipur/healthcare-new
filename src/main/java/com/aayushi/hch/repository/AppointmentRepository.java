package com.aayushi.hch.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.aayushi.hch.entity.Appointment;
import com.aayushi.hch.entity.DoctorSpeciality;

public interface AppointmentRepository extends JpaRepository<Appointment, Long> {

	public List<Appointment> findByUserId(Long id);

	public List<Appointment> findByAppointmentDateAndDoctorSpecialityAndStatusTrue(Date date, DoctorSpeciality doctorSpeciality);

	public List<Appointment> findByemailSentFalseAndAppointmentDate(Date date);
	
	public List<Appointment> findByUserIdAndAppointmentDate(Long id ,Date date );
	
	public List<Appointment> findByUserIdAndStatusTrue(Long id);
	
	public List<Appointment> findByisVisitedFalseAndAppointmentDateAndStatusTrue(Date date);
}
