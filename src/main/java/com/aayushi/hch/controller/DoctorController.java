package com.aayushi.hch.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.aayushi.hch.dao.AppointmentDao;
import com.aayushi.hch.dao.UserDao;
import com.aayushi.hch.entity.Appointment;
import com.aayushi.hch.entity.User;
import com.aayushi.hch.request.AppointmentDto;
import com.aayushi.hch.request.DateDto;

@Controller
public class DoctorController {
	@Autowired
	AppointmentDao appointmentDao;
	@Autowired
	UserDao userDao;

	@RequestMapping(method = RequestMethod.GET, value = "/DoctorAppointment")
	// userId is fetched from the login
	public ModelAndView getAllAppointment(@ModelAttribute("userId") Long userId) {

		User user = userDao.getById(userId);
		
Date date = new Date();
		List<Appointment> allAppointment = appointmentDao.getByDateAndDoc(date, user.getDoctorSpeciality());
		

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("DoctorAppointment");
		modelAndView.addObject("allAppointment", allAppointment);
		modelAndView.addObject("Id", userId);
		if (!allAppointment.isEmpty()) {
			modelAndView.addObject("userId", allAppointment.get(0).getUser().getId());
		}
		return modelAndView;

	}
	@RequestMapping(method = RequestMethod.GET, value = "/EnterDate")
	public ModelAndView getAppointment(@RequestParam Long userId) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("EnterDate");
		modelAndView.addObject("userId", userId);
		return modelAndView;

	}
	@RequestMapping(method = RequestMethod.POST, value = "/upcomingAppointment")
	public ModelAndView getupcomingAppointment(@ModelAttribute("userId") Long userId, @ModelAttribute("dateDto") DateDto date) {

		User user = userDao.getById(userId);
		

		List<Appointment> allAppointment =appointmentDao.getByDateAndDoc(date.getAppointment_date(), user.getDoctorSpeciality());
		

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("UpcomingAppointment");
		modelAndView.addObject("allAppointment", allAppointment);
		modelAndView.addObject("userId", userId);
		if (!allAppointment.isEmpty()) {
			modelAndView.addObject("userId", allAppointment.get(0).getUser().getId());
		}
		return modelAndView;

	}
	@RequestMapping(method = RequestMethod.GET, value = "/UserDetails")
	public ModelAndView getUserDetails(@RequestParam Long id ,@RequestParam Long userId ) {
		Appointment appointment = appointmentDao.getById(id);
		
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("UserDetails");
		modelAndView.addObject("appointment", appointment);
		modelAndView.addObject("userId", userId);
		return modelAndView;

	}
	@RequestMapping(method = RequestMethod.POST, value = "/visited")
	public ModelAndView saveVisited(@ModelAttribute("appointmentId") Long appointmentId ,@ModelAttribute("isVisited")String isVisited,@ModelAttribute ("userId")Long userId ) {
		
		Appointment appointment = appointmentDao.getById(appointmentId);
		appointment.setVisited(Boolean.valueOf(isVisited));
	
		appointmentDao.saveAppointment(appointment);
		ModelAndView modelAndView = new ModelAndView("redirect:" + "/DoctorAppointment");
		modelAndView.addObject("userId", userId);
		return modelAndView;
	}
}