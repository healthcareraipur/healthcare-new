package com.aayushi.hch.request;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class ReportDto {

    private Long id;
    private Long userId;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date;
    private String bloodGroup;

    private Float hemoglobin;
    private Float rbcCount;
    private Long diaBP;
    private  Long sysBP;
    private Long fastingSugar;
    private Long afterMealSugar;
    private Long platelet;
    private Float calcium ;
    private  Float thyroid_T3;
    private  Float thyroid_T4;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public Float getHemoglobin() {
        return hemoglobin;
    }

    public void setHemoglobin(Float hemoglobin) {
        this.hemoglobin = hemoglobin;
    }

    public Float getRbcCount() {
        return rbcCount;
    }

    public void setRbcCount(Float rbcCount) {
        this.rbcCount = rbcCount;
    }

    public Long getDiaBP() {
        return diaBP;
    }

    public void setDiaBP(Long diaBP) {
        this.diaBP = diaBP;
    }

    public Long getSysBP() {
        return sysBP;
    }

    public void setSysBP(Long sysBP) {
        this.sysBP = sysBP;
    }

    public Long getFastingSugar() {
        return fastingSugar;
    }

    public void setFastingSugar(Long fastingSugar) {
        this.fastingSugar = fastingSugar;
    }

    public Long getAfterMealSugar() {
        return afterMealSugar;
    }

    public void setAfterMealSugar(Long afterMealSugar) {
        this.afterMealSugar = afterMealSugar;
    }

    public Long getPlatelet() {
        return platelet;
    }

    public void setPlatelet(Long platelet) {
        this.platelet = platelet;
    }

    public Float getCalcium() {
        return calcium;
    }

    public void setCalcium(Float calcium) {
        this.calcium = calcium;
    }

    public Float getThyroid_T3() {
        return thyroid_T3;
    }

    public void setThyroid_T3(Float thyroid_T3) {
        this.thyroid_T3 = thyroid_T3;
    }

    public Float getThyroid_T4() {
        return thyroid_T4;
    }

    public void setThyroid_T4(Float thyroid_T4) {
        this.thyroid_T4 = thyroid_T4;
    }
}
