package com.aayushi.hch.utility;

import com.aayushi.hch.entity.Prescription;
import com.aayushi.hch.entity.Prescription.Medicine;
import com.aayushi.hch.entity.Report;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;
import org.springframework.stereotype.Component;

@Component
public class PDFUtility {
    public void generatePdf(Report report, String pdfPath){
    	 Document document = new Document();
    	 try
	      {
    		 String name = report.getUser().getFullName();
    		 
    		 
    		 
	         PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfPath));
	         document.open();
	        // Image image = Image.getInstance();
	         Paragraph preface = new Paragraph("HealthCAre Portal" , FontFactory.getFont(FontFactory.HELVETICA_OBLIQUE, 18,Font.BOLD, BaseColor.BLUE)); 
	         preface.setAlignment(Element.ALIGN_CENTER);
	         document.add(preface);
	         document.add(new Paragraph(" EMAIL US : healthcareraipur@gmail.com"));
	         
	         document.add(new Paragraph(new Date().toString()));
	         document.add(new Paragraph("______________________________________________________________________"));
	   
	         document.add(new Paragraph("SPECIMEN INFORMATION"));
	         document.add(new Paragraph("NAME : " +report.getUser().getFullName()));
	         document.add(new Paragraph("DOB : "+ report.getUser().getDob()));
	         document.add(new Paragraph("GENDER : "+ report.getUser().getGender()));
		        
	         document.add(new Paragraph("HEIGHT :"+ report.getUser().getHeight()));
	         document.add(new Paragraph("WEIGHT :"+ report.getUser().getWeight()));
	        
	         document.add(new Paragraph("PHONE NO. :"+ report.getUser().getMobile()));
	         document.add(new Paragraph("ADDRESS : "+ report.getUser().getAddress()));
	         document.add(new Paragraph("______________________________________________________________________"));
	         PdfPTable table = new PdfPTable(3);
	         addTableHeader(table);
	         addRows(table , report);
	         document.add(table);
	         document.close();
	         writer.close();
	      } catch (DocumentException e)
	      {
	         e.printStackTrace();
	      } catch (FileNotFoundException e)
	      {
	         e.printStackTrace();
	      }

    }
    
    private static void addTableHeader(PdfPTable table) {
	    Stream.of(" Type", " count", "Standard range")
	      .forEach(columnTitle -> {
	        PdfPCell header = new PdfPCell();
	        header.setBackgroundColor(BaseColor.LIGHT_GRAY);
	        header.setBorderWidth(2);
	        header.setPhrase(new Phrase(columnTitle));
	        table.addCell(header);
	    });
	}

    private static void addTableHeaderP(PdfPTable table) {
	    Stream.of(" Medicine Name", " Quantity", "Before/After", " No. of times" )
	      .forEach(columnTitle -> {
	        PdfPCell header = new PdfPCell();
	        header.setBackgroundColor(BaseColor.LIGHT_GRAY);
	        header.setBorderWidth(2);
	        header.setPhrase(new Phrase(columnTitle));
	        table.addCell(header);
	    });
	}

 private static void addRows(PdfPTable table, Report report) {
	 
	  PdfPCell cell = new PdfPCell(new Paragraph("Test Report"));
	  cell.setColspan(3);
	  cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	  cell.setBackgroundColor(BaseColor.ORANGE);
	  table.addCell(cell);
	  
	  
       PdfPCell afterMeal = new PdfPCell(new Paragraph(String.valueOf(report.getAfterMealSugar())));
	    table.addCell("Glucose level (After meal)");
	    table.addCell(afterMeal);
	    table.addCell("");
	   
	    PdfPCell bloodGroup = new PdfPCell(new Paragraph(String.valueOf(report.getBloodGroup())));
		   
	    table.addCell("Blood Group");
	    table.addCell(bloodGroup);
	    table.addCell("");
        
	    PdfPCell calcium = new PdfPCell(new Paragraph(String.valueOf(report.getCalcium())));
		   
	    table.addCell("Calcium");
	    table.addCell(calcium);
	    table.addCell("");
	    
	    PdfPCell diaBP = new PdfPCell(new Paragraph(String.valueOf(report.getDiaBP())));
		    
	    table.addCell("Diabp");
	    table.addCell(diaBP);
	    table.addCell("");
        
	    PdfPCell fasting = new PdfPCell(new Paragraph(String.valueOf(report.getFastingSugar())));
		   
	    table.addCell("Glucose Level (Fasting)");
	    table.addCell(fasting);
	    table.addCell("");	
        
	    PdfPCell haemoglobin  = new PdfPCell(new Paragraph(String.valueOf(report.getHemoglobin())));
		   
	    table.addCell("Haemoglobin");
	    table.addCell(haemoglobin);
	    table.addCell("");
	    
	    PdfPCell bloodPlatlets = new PdfPCell(new Paragraph(String.valueOf(report.getPlatelet())));
		   
	    table.addCell("Blood Platlets");
	    table.addCell(bloodPlatlets);
	    table.addCell("");
	    
	    PdfPCell rbc = new PdfPCell(new Paragraph(String.valueOf(report.getRbcCount())));
		       
	    table.addCell("Rbc ");
	    table.addCell(rbc);
	    table.addCell("");
 
	    PdfPCell sysBP = new PdfPCell(new Paragraph(String.valueOf(report.getSysBP())));
		   
	    table.addCell("SysBP");
	    table.addCell(sysBP);
	    table.addCell("");
	    
	    PdfPCell t3 = new PdfPCell(new Paragraph(String.valueOf(report.getThyroid_T3())));
		   
	    table.addCell("T3");
	    table.addCell(t3);
	    table.addCell("");
        
	    PdfPCell t4 = new PdfPCell(new Paragraph(String.valueOf(report.getThyroid_T4())));
		   
	    table.addCell("T4");
	    table.addCell(t4);
	    table.addCell("");
 }
 
public void generatePdf(Prescription prescription, String pdfPath){
	 Document document = new Document();
	 
	 try
	  {

	     PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfPath));
	     document.open();
	    
	     Paragraph preface = new Paragraph("HealthCAre Portal" , FontFactory.getFont(FontFactory.HELVETICA_OBLIQUE, 18,Font.BOLD, BaseColor.BLUE)); 
	     preface.setAlignment(Element.ALIGN_CENTER);
	     document.add(preface);
	     document.add(new Paragraph(" EMAIL US : healthcareraipur@gmail.com"));
	     
	     document.add(new Paragraph(new Date().toString()));
	    
	     document.add(new Paragraph("______________________________________________________________________"));

	     document.add(new Paragraph("SPECIMEN INFORMATION"));
	     document.add(new Paragraph("NAME : " +prescription.getPatientName()));
	     document.add(new Paragraph("AGE "+ prescription.getAge()));
	     document.add(new Paragraph("GENDER :"+prescription.getGender()));
	     document.add(new Paragraph("______________________________________________________________________"));
	   
	    PdfPTable table = new PdfPTable(4);
	     addTableHeaderP(table);
	     addRowsP(table,prescription);
         document.add(table);
	     document.close();
	     writer.close();
	  } catch (DocumentException e)
	  {
	     e.printStackTrace();
	  } catch (FileNotFoundException e)
	  {
	     e.printStackTrace();
	  }
 }
 
 
private static void addRowsP(PdfPTable table, Prescription prescription) {
	 
	  PdfPCell cell = new PdfPCell(new Paragraph("Prescription "));
	  cell.setColspan(4);
	  cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	  cell.setBackgroundColor(BaseColor.ORANGE);
	  table.addCell(cell);
	  List<Medicine> medicines = prescription.getMedicines();
	 
	  for(int i=0;i<medicines.size();i++ ) {
		  
		  table.addCell(String.valueOf(medicines.get(i).getName()));
		  table.addCell(String.valueOf(medicines.get(i).getQuantity()));
		  table.addCell(String.valueOf(medicines.get(i).getTake()));
		  table.addCell(String.valueOf(medicines.get(i).getFrequency()));
			  
		  
	  }
      }

}
 
