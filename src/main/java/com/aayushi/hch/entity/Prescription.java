package com.aayushi.hch.entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Prescription {

	private String patientName;
	private String gender;
	private Long age;
	private List<Medicine> medicines;

	@Override
	public String toString() {
		return "Prescription [patientName=" + patientName + ", gender=" + gender + ", age=" + age + ", medicines="
				+ medicines + "]";
	}

	public Prescription(String patientName, String gender, Long age, List<Medicine> medicines) {
		super();
		this.patientName = patientName;
		this.gender = gender;
		this.age = age;
		this.medicines = medicines;
	}

	public Prescription() {
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Long getAge() {
		return age;
	}

	public void setAge(Long age) {
		this.age = age;
	}

	public List<Medicine> getMedicines() {
		return medicines;
	}

	public void setMedicines(List<Medicine> medicines) {
		this.medicines = medicines;
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Medicine {
		private String name;
		private String quantity;
		private String frequency;
		private String take;

		public Medicine(String name, String quantity, String frequency, String take) {
			this.name = name;
			this.quantity = quantity;
			this.frequency = frequency;
			this.take = take;
			
		}

		public Medicine() {
		}

		@Override
		public String toString() {
			return "Medicine [ name=" + name + ", quantity=" + quantity + ", frequency=" + frequency
					+ ", take=" + take + "]";
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getQuantity() {
			return quantity;
		}

		public void setQuantity(String quantity) {
			this.quantity = quantity;
		}

		public String getFrequency() {
			return frequency;
		}

		public void setFrequency(String frequency) {
			this.frequency = frequency;
		}

		public String getTake() {
			return take;
		}

		public void setTake(String take) {
			this.take = take;
		}

	}

}
