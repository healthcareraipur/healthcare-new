package com.aayushi.hch.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.aayushi.hch.entity.Comment;
import com.aayushi.hch.repository.CommentRepository;

@Repository
public class CommentDao {
	
	@Autowired
	CommentRepository commentRepository;
	
	public void saveComment(Comment comment) {
		commentRepository.save(comment);
	}
	
	public List<Comment> getAllComments(){
		return commentRepository.findAllByOrderByCreatedOnDesc();
	}
	
	public Comment getById(Long commentId) {
		return commentRepository.findOne(commentId);
	}

}
