<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Insert title here</title>

<link rel="stylesheet" href="css/MainPage.css"/>
</head>
<body>




<header>
<div class="container">
<div id="branding">
<h2><span class="highlight">Patient Satisfactory & Informative Government HealthCare Web Portal </span><span class="second"> </span></h2>
 </div>

</div>
</header>

<nav class="navbar navbar-inverse"style="background-color:#00688B;">
  <div class="container-fluid">
    <div class="navbar-header">
    </div>
    <ul class="nav navbar-nav">
     </ul>
    <ul class="nav navbar-nav navbar-right">
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Doctors Available
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">Dr.Mehta , Neurologist</a></li>
          <li><a href="#">Dr. Abbas,child specialist</a></li>
          <li><a href="#">Dr. Shetty,heart specialist</a></li>
        </ul>
      </li>
     
      <li><a href="http://localhost:8080/register"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
      <li><a href="http://localhost:8080/loginHere"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
      <li><a href="http://localhost:8080/FAQ">FAQ</a></li>
      
    </ul>
  </div>
</nav>
<div class="jumbotron">
<div class="container">

</div>
  <h1 >Health Tips <i class="fi-lightbulb"></i> :</h1>
  <p class="lead">Take a balanced diet which is rich in nutrients, fibers & vitamins.</p>
  <hr class="my-4">
  <p>Enrich your diet with amla,honey,ginger,and garlic as they are beneficial for your health.</p>
  <p class="lead">
    <a class="btn btn-primary btn-lg" href="http://localhost:8080/loginHere" role="button">Learn more</a>
  </p>
  </div>
</div>
</div>




<div class="mega">
<div class="row">
  <div class="col-sm-6 ">
    <div class="card">
      <div class="card-body">
        <h3 class="card-title">About us</h3>
        <p class="card-text">GOVERNMENT HEALTHCARE PORTAL DEVELOPS<br>This web portal has been develop to provide health related information and government plans,events , med camps info.It also provides you easiest way to get appointments</p>
        <a href="http://localhost:8080/loginHere" class="btn btn-primary">Go somewhere</a>
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
        <h3 class="card-title">Our Mission</h3>
        <p class="card-text">To provide you a user friendly portal.
        <br>To create a general awareness about health and spread govt. scheme and events informations.
             <br>Get you appointments in easiest way.
             <br></p>
        <a href="http://localhost:8080/loginHere" class="btn btn-primary">Go somewhere</a>
      </div>
    </div>
  </div>
</div>
</div>











<!-- Footer -->
<footer class="page-footer font-small blue-grey lighten-5">

    <div style="background-color: #21d192;">
      <div class="container">

        <!-- Grid row-->
        <div class="row py-4 d-flex align-items-center">

          <!-- Grid column -->
          <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
            <h6 class="mb-0">Get connected with us </h6>
          </div>
          <!-- Grid column -->


        </div>
        <!-- Grid row-->

      </div>
    </div>

    <!-- Footer Links -->
    <div class="container text-center text-md-left mt-5">

      <!-- Grid row -->
      <div class="row mt-3 dark-grey-text">

        <!-- Grid column -->
        <div class="col-md-3 col-lg-4 col-xl-3 mb-4">

          <!-- Content -->
          <h6 class="text-uppercase font-weight-bold">Government HealthCare Portal develops</h6>
          <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
          <p>This web portal has been develop to provide health related information and government plans,events , med camps info.It also provides you easiest way to get appointments </p>

        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">

          <!-- Links -->
          <h6 class="text-uppercase font-weight-bold"></h6>
          <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
          <p>
            <a class="dark-grey-text" href="#!"></a>
          </p>
          <p>
            <a class="dark-grey-text" href="#!"></a>
          </p>
          <p>
            <a class="dark-grey-text" href="#!"></a>
          </p>
          <p>
            <a class="dark-grey-text" href="#!"></a>
          </p>

        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">

          <!-- Links -->
          <h6 class="text-uppercase font-weight-bold"></h6>
          <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
          <p>
            <a class="dark-grey-text" href="#!"></a>
          </p>
          <p>
            <a class="dark-grey-text" href="#!"></a>
          </p>
          <p>
            <a class="dark-grey-text" href="#!"></a>
          </p>
          <p>
            <a class="dark-grey-text" href="#!"></a>
          </p>

        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">

          <!-- Links -->
          <h6 class="text-uppercase font-weight-bold">Contact</h6>
          <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
          <p>
            <i class="fa fa-home mr-3"></i> Raipur, CG 10012, India</p>
          <p>
            <i class="fa fa-envelope mr-3"></i> healthCareRaipur@gmail.com</p>
          <p>
            <i class="fa fa-phone mr-3"></i> + 01 234 567 88</p>
          <p>
            <i class="fa fa-print mr-3"></i> + 01 234 567 89</p>

        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row -->

    </div>
    <!-- Footer Links -->

    <!-- Copyright -->
    <div class="footer-copyright text-center text-black-50 py-3">© 2018 Copyright:
      <a class="dark-grey-text" href="https://mdbootstrap.com/bootstrap-tutorial/"> govHealthcare.com</a>
    </div>
    <!-- Copyright -->

  </footer>
  <!-- Footer -->





</body>



</html>