package com.aayushi.hch.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.aayushi.hch.entity.Comment;
import com.aayushi.hch.entity.Reply;

public interface ReplyRepository extends JpaRepository<Reply, Long>{

	public List<Reply> findByCommentIdOrderByCreatedOnDesc(Comment id);
}
