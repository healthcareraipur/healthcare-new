<!DOCTYPE html>
<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular.min.js"></script>
    <style>
        div{
            font:15px Verdana;
            width:450px;
        }
        ul {
            padding:0;
            margin:2px 5px; 
            list-style:none; 
            border:0; 
            float:left; 
            width:100%;
        }
        li {
            width:50%; 
            float:left; 
            display:inline-block; 
        }
        table, input {
            text-align:left;
            font:13px Verdana;
        }
        table, td, th 
        {
            margin:10px 0;
            padding:2px 10px;
        }
        td, th {
            border:solid 1px #CCC;
        }
        button {
            font:13px Verdana;
            padding:3px 5px;
        }

    </style>
<link rel="stylesheet" href="css/Register.css"/>

</head>
<body>


    <div ng-app="myApp" ng-controller="myController">
        
        
        	
        
        <ul>
            <li>Patient Name</li>
            <li><input type="text" ng-model="patientname" /></li>
        </ul>
         
        <ul>
            <li>Age </li>
            <li><input type="text" ng-model="age" /></li>
        </ul>
        <ul>
            <li>Gender</li>
            <li><input type="text" ng-model="gender" /></li>
        </ul>
        <ul>
            <li>Medicine Name</li>
            <li><input type="text" ng-model="name" /></li>
        </ul>
        <ul>
            <li>Quantity</li>
            <li><input type="text" ng-model="quantity" /></li>
        </ul>
        <ul>
            <li>Times a day</li>
            <li><input type="text" ng-model="frequency" /></li>
        </ul>
        <ul>
            <li>Before/After meal</li>
            <li><input type="text" ng-model="take" /></li>
        </ul>
        <ul>
            <li><button ng-click="addRow()"> Add Row </button></li>
        </ul>

        <!--CREATE A TABLE-->
        <table> 
            <tr>
                <th>Code</th>
                    <th>Medicine Name</th>
                        <th>Quantity</th>
                        <th>Times a day</th>
                        <th>Before/After meal</th>
            </tr>

            <tr ng-repeat="medicine in medicineArray">
                <td><label>{{$index + 1}}</label></td>
                <td><label>{{medicine.name}}</label></td>
                <td><label>{{medicine.quantity}}</label></td>
                <td><label>{{medicine.frequency}}</label></td>
                <td><label>{{medicine.take}}</label></td>
                <td><input type="checkbox" ng-model="medicine.Remove"/></td>
            </tr>
        </table>
  
        <div>
            <button ng-click="submit()">Submit Data</button> 
                <button ng-click="removeRow()">Remove Row</button>
        </div>

        
    </div>
 
</body>

<!--The Controller-->
<script>
    var app = angular.module('myApp', []);
    app.controller('myController', function ($scope) {

        // JSON ARRAY TO POPULATE TABLE.
        $scope.medicineArray =
        [
            
        ];
        $scope.patientname;
        $scope.age;
        $scope.gender;
        // GET VALUES FROM INPUT BOXES AND ADD A NEW ROW TO THE TABLE.
        $scope.addRow = function () {
            if ($scope.name != undefined && $scope.quantity != undefined && $scope.frequency != undefined && $scope.take != undefined) {
                var medicine = [];
                medicine.name = $scope.name;
                medicine.quantity = $scope.quantity;
                medicine.frequency = $scope.frequency;
                medicine.take = $scope.take;
                $scope.medicineArray.push(medicine);

                // CLEAR TEXTBOX.
                $scope.name = null;
                $scope.quantity = null;
                $scope.frequency = null;
                $scope.take = null;
            }
        };
    
        // REMOVE SELECTED ROW(s) FROM TABLE.
        $scope.removeRow = function () {
            var arrMedicine = [];
            angular.forEach($scope.medicineArray, function (value) {
                if (!value.Remove) {
                	arrMedicine.push(value);
                }
            });
            $scope.medicineArray = arrMedicine;
        };

        // FINALLY SUBMIT THE DATA.
        $scope.submit = function () {
           // var arrMovie = [];
           var req = { 
              medicines:[]
                };
            angular.forEach($scope.medicineArray, function (value) {
                req.medicines.push( { name: value.name  , quantity: value.quantity  , frequency:value.frequency  , take: value.take } );
            });
           req.patientName=$scope.patientname;
           req.age=$scope.age;
           req.gender=$scope.gender;
          
          
          
        //   $scope.display = req;
          
            console.log(req);
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.open("post", "http://localhost:8080/saveMedicine");
            xmlHttp.send(JSON.stringify(req));
        };
    });
</script>
</html>