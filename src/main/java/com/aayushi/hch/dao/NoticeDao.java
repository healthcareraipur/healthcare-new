package com.aayushi.hch.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.aayushi.hch.entity.Notice;
import com.aayushi.hch.repository.NoticeRepository;

@Repository
public class NoticeDao {
	@Autowired
	private NoticeRepository noticeRepository;

	public void saveNotice(Notice notice) {
		noticeRepository.save(notice);
	}

	public Notice getById(Long id) {
		return noticeRepository.findOne(id);
	}

	public List<Notice> getAllNotices() {
		return noticeRepository.findAll();
	}

	public void updateNotice(Notice notice, Long id) {
		Notice findOne = noticeRepository.findOne(id);
		if (findOne == null) {
			throw new IllegalArgumentException("Notice doesn't exist with id=" + id);
		}
		notice.setId(id);
		noticeRepository.save(notice);

	}

	public void deleteNotice(Long id) {
		noticeRepository.delete(id);
		
	}
}
