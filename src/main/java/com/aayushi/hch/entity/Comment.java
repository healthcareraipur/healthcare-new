package com.aayushi.hch.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Comment {
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private Long commentId;
	
	private String topic;
	private String description;
	
	@ManyToOne
	@JoinColumn(name = "createdBy", nullable = false)
	private User createdBy;
	
	@Temporal(TemporalType.DATE)
	private Date createdOn;
	
	@Temporal(TemporalType.DATE)
	private Date modifiedOn;
	
	

	public Comment(Long commentId, String topic, String description, User createdBy, Date createdOn, Date modifiedOn) {
		super();
		this.commentId = commentId;
		this.topic = topic;
		this.description = description;
		this.createdBy = createdBy;
		this.createdOn = createdOn;
		this.modifiedOn = modifiedOn;
	}
	
	public Comment() {}

	public Long getCommentId() {
		return commentId;
	}

	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	@Override
	public String toString() {
		return "Comment [commentId=" + commentId + ", topic=" + topic + ", description=" + description + ", createdOn="
				+ createdOn + ", createdBy=" + createdBy + ", modifiedOn=" + modifiedOn + "]";
	}
	
	

}
