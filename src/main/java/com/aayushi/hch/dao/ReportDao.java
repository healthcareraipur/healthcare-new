package com.aayushi.hch.dao;

import com.aayushi.hch.entity.Report;
import com.aayushi.hch.repository.ReportRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ReportDao {
    @Autowired
    private ReportRepository reportRepository;

    public void saveReport(Report report){
        reportRepository.save(report);
    }
    
    public List<Report> getById(Long userId){
    	return reportRepository.findByUserId(userId);
    }
}
