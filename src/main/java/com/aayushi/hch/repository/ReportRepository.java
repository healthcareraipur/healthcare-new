package com.aayushi.hch.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.aayushi.hch.entity.Report;

public interface ReportRepository extends JpaRepository<Report, Long> {

	public List<Report> findByUserId(Long id);
}
