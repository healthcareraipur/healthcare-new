<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="css/Comment.css"/>
</head>
<body style="background-color:#00688B;">
<center><font color="white"><H1><b>Health Care FAQ</b></H1></font></center>

 <div class="faq" >

<br><font color="blue">
What state agency regulates HMOs?<br> </font>
The Department of Banking and Insurance regulates HMOs.
<br><br>
<font color="blue">
Where can I find a list of HMOs authorized in New Jersey?<br></font>
The Department of Banking and Insurance provides names, addresses, telephone numbers, and Web sites of HMOs authorized in New Jersey.
<br><br>
<font color="blue">
Where can I find information about different HMO plans?<br></font>
The Department of Banking and Insurance issues a report card on HMO plans.
<br><br>
<font color="blue">
How do I make a complaint about an HMO plan?<br></font>
An individual wishing to make a complaint about an HMO may file a complaint as noted on the New Jersey Department of Banking and Insurance website.
<br><br>
<font color="blue">
Where can I find information on low-cost health insurance for myself and my children?<br></font>
NJ FamilyCare provides information that will help you determine whether you qualify for low-cost or no-cost health insurance. You can also download an application or view frequently asked questions.
<br><br>
<font color="blue">
Where can I find low-cost health care for myself and my family?<br></font>
New Jersey has a network of community health centers that offer affordable health care for the entire family.  Health centers serve the uninsured and underinsured, and also take patients with Medicaid, Medicare and private insurance.
<br><br>
<font color="blue">
What immunizations are required for my child to start school?<br></font>
Information about immunizations for school children is available from the Department of Health and Senior Services.
<br><br>
<font color="blue">
How can I find out if I qualify for prescription assistance?<br></font>
The Pharmaceutical Assistance to the Aged & Disabled (PAAD) page includes links to information about pharmaceutical assistance programs, including eligibility requirements.
<br><br>
<font color="blue">
Where can I find information on nursing homes and nursing home alternatives?<br></font>
The Department of Health and Senior Services has information about nursing home facilities, Assisted Living Facilities, Adult Medical Day Care, Residential Health Care, Alternative Family Care, on their long-term care facility database.  Detailed information on nursing homes performance is available from the federal Nursing Home Compare web site.
<br><br>
<font color="blue">
Where can I find locations to donate blood?<br></font>
The addresses and phone numbers of New Jersey's licensed blood banks are listed online by the Department of Health and Senior Services.
<br><br>
<font color="blue">
How do I contact Medicaid?<br></font>
The Health Care Financing Administration provides state and federal Medicaid contact lists.
<br><br>
<font color="blue">
How do I contact Medicare?<br></font>
The Medicare Web site allows you to search for helpful contacts.
<br><br>
<font color="blue">
Where do I get a list of acute care health care facilities?<br></font>
The Department of Health and Senior Services, Health Care Systems Analysis division, maintains an interactive web site that permits searches by type of facility and location.
<br><br>
<font color="blue">
Where can I find information about the performance of all New Jersey acute care hospitals?<br></font>
The Department of Health and Senior Services has developed a report card that contains information on the performance of all New Jersey hospitals for heart attack and pneumonia.
<br><br>
<font color="blue">
How do I make a complaint about a health care facility?<br></font>
Complaints may be filed online, by phone, by fax, or by mail. Visit the Department of Health and Senior Services webpage to learn more.
<br><br>
<font color="blue">
What state agency regulates acute health care facilities?<br></font>
The Department of Health and Senior Services, Division of Health Facilities Evaluation and Licensing investigates complaints related to a facility's adherence to quality of care standards set forth in statute and regulation.
<br><br>
<font color="blue">
What state agency licenses acute care facilities?<br></font>
The Department of Health and Senior Services, Health Care Quality and Oversight division, licenses facilities in accordance with the state's statues and regulations.
<br><br>
<font color="blue">
When I need hospital care and cannot pay for it, can the state help me?<br></font>
The New Jersey Hospital Care Payment Assistance Program (Charity Care Assistance) is available for people who are uninsured or underinsured.  Patients may qualify for free or reduced charge services for necessary hospital care, although some services that are separate from hospital charges (physician fees, anesthesiology fees, radiology interpretation, and outpatient prescriptions, for example) may not be eligible for reduction. Patients will be considered for this program only after being determined ineligible for all other medical assistance programs and/or other potential payers, since it is the payer of last resort.
<br><br>
<font color="blue">
Where do I get information about hospitals and physicians where cardiac surgery is available?<br></font>
The Department of Health and Senior Services, Office of Health Care Quality Assessment,  annually publishes a Report Card on cardiac surgery results for both hospitals and surgeons.
</div>
</body>
</html>