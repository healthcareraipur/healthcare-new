<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js'></script>
<title>Insert title here</title>
<style>



.canvas-container {
max-height: 300px;
max-width: 500px;

    }


   #bp {
height: 200px;
width: 600px;
position: absolute;
left: 30%;
top: 15%;
 }
 
 
   #diabetes {
height: 200px;
width: 600px;
position: absolute;
left: 30%;
top: 80%;
 }
 
   #blood {
height: 200px;
width: 600px;
position: absolute;
left: 30%;
top: 145%;
 }
 
  #blood2 {
height: 200px;
width: 600px;
position: absolute;
left: 30%;
top: 210%;
 }
 
   #calcium {
height: 200px;
width: 600px;
position: absolute;
left: 30%;
top: 275%;
 }
 
   #thyroid {
height: 200px;
width: 600px;
position: absolute;
left: 30%;
top: 340%;
 }
 
  #thyroid2 {
height: 200px;
width: 600px;
position: absolute;
left: 30%;
top: 405%;
 }
 
 
 
 
 
</style>


</head>
<body>
	<H1><center>Health Progress Chart</center></H1>
	 <!-- line chart canvas element -->

<div id="lineGraph" class="canvas-container" > <!--opening lineGraph-->
 
        <canvas id="bp" width="600" height="400"></canvas>     

    </div>



<div id="lineGraph" class="canvas-container"> <!--opening lineGraph-->

   
      <center>          <canvas id="diabetes" width="600" height="400"></canvas> </center>  
          

    </div>

 
<div id="lineGraph" class="canvas-container"> <!--opening lineGraph-->
      <center> <canvas id="blood" width="600" height="400"></canvas> </center>
      
    </div>
     
      
<div id="lineGraph" class="canvas-container"> <!--opening lineGraph-->
      <center> <canvas id="blood2" width="600" height="400"></canvas> </center>
      
    </div>
     
    
   
    <div id="lineGraph" class="canvas-container"> <!--opening lineGraph-->
     
      <center>   <canvas id="calcium" width="600" height="400"></canvas> </center>
      
    </div>
 

   
    <div id="lineGraph" class="canvas-container"> <!--opening lineGraph-->
      <center>   <canvas id="thyroid" width="600" height="400"></canvas> </center>

    </div>
    <div id="lineGraph" class="canvas-container"> <!--opening lineGraph-->
      <center>   <canvas id="thyroid2" width="600" height="400"></canvas> </center>

    </div>

    
      <script>
            // line chart data
            var report = ${reports}
            var i ;
            console.log(report);
            var date=[];
            var bp=[];
            var bp2=[];
            var diabetes=[];
            var diabetes2=[];
            var rcb=[];
            var platlets=[];
            var haemo=[];
            var cal=[], thy3=[],thy4=[];
            for(i=0;i<report.length;i++){
            	
            	date.push(report[i].date);
            	bp.push(report[i].diaBP);
            	bp2.push(report[i].sysBP);
            	
            	diabetes.push(report[i].fastingSugar);
            	diabetes2.push(report[i].afterMealSugar);
                
            	rcb.push(report[i].rbcCount);
            	platlets.push(report[i].platelet);
            	haemo.push(report[i].hemoglobin);
            	
            	cal.push(report[i].calcium);
            	
                
            	thy3.push(report[i].thyroid_T3);
            	thy4.push(report[i].thyroid_T4);
            	console.log(cal);
            }
            
           
            
            // get line chart canvas
            var bp3= document.getElementById('bp').getContext('2d');
            var bp1 = new Chart(bp3 , {
                type: "line",
                data: {
                	labels : date,
                    datasets : [
                        {   label:'DIABOLIC BLOODPRESSURE',
                            borderColor: 'black',
                            backgroundColor:'#ff944d',
                            data : bp
                            
                        },
                        
                        {   label:'SYSTOLIC BLOODPRESSURE',
                            borderColor: 'black',
                            backgroundColor:'#ffcce6',
                            data : bp2
                        }
                     
                    ] }, 
                    
                    options:{
                    	 title: {
                	            display: true,
                	            text: 'BLOOD PRESSURE CHART'
                	        },
                    	legend: {
                    		display: true,
                    		 position: 'top',
                    		  labels: {
                    	    	  boxWidth :10,
                    	        fontColor: "#000523",
                    	      }
                    	            }
                    	}
                    
            
            });            
            
            
            
            
            
            
            var diabetes1= document.getElementById('diabetes').getContext('2d');
            var myNewChart = new Chart(diabetes1, {
                type: "line",
                data: {
                	   labels : date,
                       datasets : [
                       {   label:'SUGAR LEVEL BEFORE MEAL',
                           borderColor: 'black',
                           backgroundColor:'#cce6ff',
                           data : diabetes
                           
                       },
                       {    label:'SUGAR LEVEL AFTER MEAL',
                           borderColor: 'black',
                           backgroundColor:'#ffcccc', 
                           data : diabetes2
                       }
                       
                   ]
                },
                
                options:{
                	 title: {
            	            display: true,
            	            text: 'SUGAR LEVEL CHART'
            	        },
                	legend: {
                		display: true,
                		 position: 'top',
                		 
                	      labels: {
                	    	  boxWidth :10,
                	        fontColor: "#000523",
                	      }
                	}
                
            }
                });
            
 
            var rcb3= document.getElementById('blood').getContext('2d');
            var rcb1 = new Chart(rcb3 , {
                type: "line",
                data: {
                	 labels : date,
                	 datasets : [
                      {  label:'HEMOGLOBIN',
                         borderColor: 'black',
                         backgroundColor:'#ffff99',
                         data : haemo
                     },
                     {   label:'PLATLETS',
                         borderColor: 'black',
                         backgroundColor:'#ff531a', 
                         data : platlets
                     } 
                 ]
                }, 
                options:{
                	 title: {
            	            display: true,
            	            text: 'HEMOGLOBIN , PLATLET COUNT'
            	        },
                	legend: {
                		display: true,
                		 position: 'top',
                		 
                	      labels: {
                	    	  boxWidth :10,
                	        fontColor: "#000523",
                	      }
                	} 
                }
            });
            
            var rcb3= document.getElementById('blood2').getContext('2d');
            var rcb1 = new Chart(rcb3 , {
                type: "line",
                data: {
                	 labels : date,
                     datasets : [
                     {   label:'RBC',
                         borderColor: 'black',
                         backgroundColor:'#99ff99', 
                         data : rcb
                     }
                 ]
                }, 
                options:{
                	 title: {
            	            display: true,
            	            text: 'RBC COUNT'
            	        },
                	legend: {
                		display: true,
                		 position: 'top',
                		 
                	      labels: {
                	    	  boxWidth :10,
                	        fontColor: "#000523",
                	      }
                	} 
                }
            });
            
            
            
            
            
            
            
            var cal3= document.getElementById('calcium').getContext('2d');
            var cal1 = new Chart(cal3 , {
                type: "line",
                data: {
                    labels : date,
                    datasets : [
                    {   label:'Calcium',
                        borderColor: 'black',
                        backgroundColor:'#e0e0d1', 
                        data : cal
                    }
                 
                ]
                	
                }, 

                options:{
                	 title: {
            	            display: true,
            	            text: 'THYROID CHART'
            	        },
                	legend: {
                		display: true,
                		 position: 'top',
                		 
                	      labels: {
                	    	  boxWidth :10,
                	        fontColor: "#000523",
                	      }
                	}
                }
            });
            
            
            
            
             
             var thy= document.getElementById('thyroid').getContext('2d');
             
             var thy1 = new Chart(thy , {
                type: "line",
                height:350,
                data: {
                	
                	 labels : date,
                     datasets : [
                     {   label:'T3',
                         borderColor: 'black',
                     backgroundColor:'blue', 
                         data : thy3
                     }
                 ]
                }, 
                options:{

           		 title: {
           	            display: true,
           	            text: 'THYROID CHART(T3)'
           	        }, 
           	        
                	legend: {
                		
                		display: true,
                		 position: 'top',
                		
                	      labels: {
                	    	  boxWidth :10,
                	        fontColor: "#000523",
                	      }}}
                
                });
             
             
  var thy= document.getElementById('thyroid2').getContext('2d');
             
             var thy1 = new Chart(thy , {
                type: "line",
                height:350,
                data: {
                	
                	 labels : date,
                     datasets : [
                     
                     {   label:'T4',
                    	 borderColor: 'black',
                         backgroundColor:'#66ccff', 
                         
                         data : thy4
                     }
                 ]
                }, 
                options:{

           		 title: {
           	            display: true,
           	            text: 'THYROID CHART(T4)'
           	        }, 
           	        
                	legend: {
                		
                		display: true,
                		 position: 'top',
                		
                	      labels: {
                	    	  boxWidth :10,
                	        fontColor: "#000523",
                	      }}}
                
                });
          
             
          
           
        </script>
	
</body>
</html>